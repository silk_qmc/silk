import numpy as np
import sys
import copy
import itertools

# symmetry product of list of orbitals
def symmetry_product(orbs, sym_orb, symmetry_reduction):
  """ Return product of symmetry operations of orbitals 'orb' with symmetries 'sym_orb[orb]'
      under symmetry_reduction
  """
  sym = 0
  for orb in orbs:
    sym = symmetry_reduction[sym, sym_orb[orb]]
  return sym

def n_excitations(configuration, ne):
  """Return the number of excitations realized in this configuration
     This assumes the same order as documented in setup_spin_orbitals()
  """
  # This is implemented as counting how many otherwise unoccupied orbitals
  # are in this configuration
  return configuration[ configuration >= ne ].size

def orbital_hash(orbital):
  orbital_hash = 0
  for pos in orbital:
    orbital_hash += 1 << int(pos)
  return orbital_hash

def iterate_spin_orbitals(old_orbitals, n_orbitals, ne_up, ne_dn, sym_orb, symmetry_reduction, limit_excitations=99999):
  """ Setup all possible spin-orbital configurations that we want to consider,
      excited from old_orbitals (or the ground state if None).
      This excludes configurations that are just formed by known symmetries
  """
  ne = ne_up + ne_dn
  ne_up_empty = n_orbitals - ne_up
  ne_dn_empty = n_orbitals - ne_dn

  # Construct list of nwchem-style indexing of spin-orbitals, in ground-state configuration
  nwchem_up_orbitals_occ   = set(                           list(range(ne_up)))
  nwchem_dn_orbitals_occ   = set([x   +ne_up       for x in range(ne_dn)])
  nwchem_up_orbitals_unocc = set([x+ne             for x in range(ne_up_empty)])
  nwchem_dn_orbitals_unocc = set([x+ne+ne_up_empty for x in range(ne_dn_empty)])
  # lists of all up/down orbitals
  nwchem_up_orbitals       = nwchem_up_orbitals_occ | nwchem_up_orbitals_unocc
  nwchem_dn_orbitals       = nwchem_dn_orbitals_occ | nwchem_dn_orbitals_unocc

  if old_orbitals == None:
#    old_orbitals = [np.append(nwchem_up_orbitals_occ, nwchem_dn_orbitals_occ)]
    # temporary hack to enable only ground state and another state with the two
    # highest-energy electrons excited to the next (prev. unoccupied) orbital
    old_orbitals = [nwchem_up_orbitals_occ | nwchem_dn_orbitals_occ]
#                    [0, 1, 2, 3,10, 5, 6, 7, 8, 9],
#                    [0, 1, 2, 3,11, 5, 6, 7, 8, 9],
#                    [0, 1, 2, 3, 4, 5, 6, 7, 8,12],
#                    [0, 1, 2, 3, 4, 5, 6, 7, 8,13],
#                    [0, 1, 2, 3,10, 5, 6, 7, 8,12],
#                    [0, 1, 2, 3,10, 5, 6, 7, 8,13],
#                    [0, 1, 2, 3,11, 5, 6, 7, 8,12],
#                    [0, 1, 2, 3,11, 5, 6, 7, 8,13],
#4/1/2 gamess
# 0 2 0
#    old_orbitals.append([0, 1, 2, 3, 10, 5, 6, 7, 8, 12])
# 0 0 2
#    old_orbitals.append([0, 1, 2, 3, 11, 5, 6, 7, 8, 13])

#3/2/2
# 0 2 2 2 0
#[0, 1, 3, 4, 10, 5, 6, 8, 9, 12],
# 2 2 0 2 0
#[0, 1, 2, 3, 10, 5, 6, 7, 8, 12],
# 0 2 2 0 2
#[0, 1, 3, 4, 11, 5, 6, 8, 9, 13],
# 2 2 0 0 2
#[0, 1, 2, 3, 11, 5, 6, 7, 8, 13],
#1 2 2 1 0
#[0, 1, 2, 3, 4, 5, 6,  8, 9, 12],
#[0, 1, 3, 4, 10, 5, 6, 7, 8, 9],
#1 2 0 1 2
#[0, 1, 3, 4, 11, 5, 6, 8, 12, 13],
#[0, 1, 3, 10, 11, 5, 6, 8, 9, 13]]

    print("old: ", old_orbitals)
  else:
    # convert numpy arrays to sets
    old_orbitals = [set(x.flat) for x in old_orbitals]

  spin_orbitals   = [copy.deepcopy(list(x)) for x in old_orbitals]
  # generate orbital-hashtable to exclude duplicates later more easily
  orbitals_hash = {}
  for old_orbital in old_orbitals:
    orbitals_hash[orbital_hash(old_orbital)] = 1
  # generate new spin-orbitals from each of the previously existing spin-orbitals
  for old_orbital in old_orbitals:
    # Labeling of orbitals, to be consistent with nwchem:
    #  - occupied up
    #  - occupied dn
    #  - unoccupied up
    #  - unoccupied dn

    # now filter those that are actually occupied up/down in old_orbital
    up_orbitals_occ   = nwchem_up_orbitals & old_orbital
    dn_orbitals_occ   = nwchem_dn_orbitals & old_orbital
    up_orbitals_unocc = nwchem_up_orbitals - up_orbitals_occ
    dn_orbitals_unocc = nwchem_dn_orbitals - dn_orbitals_occ

    # Construct combinations of orbitals we want. This could be done in one line if
    #   we would consider _all_ combinations (itertools.combinations), but we might want
    #   only specific combinations, or a specific number of excitations, so we do it by
    #   hand to keep it readable.
    sym = symmetry_product(up_orbitals_occ | dn_orbitals_occ, sym_orb, symmetry_reduction)

    # The following nested loops look a bit complicated, but aren't really. First, most
    # loops are there twice: for up and down electrons. And secondlly, each loop has a
    # comment as well.

    # loop over how many e- we try to excite
    for ne_ex in 1+np.arange(min(ne, ne_up_empty+ne_dn_empty, limit_excitations)):
      # loop over distribution of up vs down e- that are excited
      for ne_ex_up in range(1+ne_ex):
        ne_ex_dn = ne_ex - ne_ex_up
        # loop over combinations of up orbitals we excite from
        for ex_up_from in itertools.combinations(up_orbitals_occ, ne_ex_up):
          # loop over combinations of dn orbitals we excite from
          for ex_dn_from in itertools.combinations(dn_orbitals_occ, ne_ex_dn):
            # loop over combinations of up orbitals we excite to
            for ex_up_to in itertools.combinations(up_orbitals_unocc, ne_ex_up):
              # loop over combinations of dn orbitals we excite to
              for ex_dn_to in itertools.combinations(dn_orbitals_unocc, ne_ex_dn):
                # construct list of orbitals from these combinations
                #print(up_orbitals_occ, dn_orbitals_occ)
                orbs_from = ((up_orbitals_occ - set(ex_up_from)) |
                             (dn_orbitals_occ - set(ex_dn_from)))
                orbs_to   = set(ex_up_to) |  set(ex_dn_to)
                orbs      = orbs_from | orbs_to
                orbs_hash = orbital_hash(orbs)
                if orbs_hash not in orbitals_hash:
#                 # check if symmetry is the same as the one of the ground state,
#                 # and only include those combinations
                  if sym == symmetry_product(orbs, sym_orb, symmetry_reduction):
                    spin_orbitals += [np.asarray(list(orbs), dtype=np.int)]
                    orbitals_hash[orbs_hash] = 1
  spin_orbitals = np.asarray(spin_orbitals, dtype=np.int)
  print("new: ", spin_orbitals)
  print("Silk: setup spin-orbitals done")
  return spin_orbitals

# calculate 'difference' between two orbitals
def orb_difference(ne_up, ne_dn, jer, k1, k2):
  ne = ne_up + ne_dn
  # First, we need to find, by how much jer(:,k1) and jer(:,k2) differ (ndiff),
  # but since we also have to place them in maximum coincidence we need to
  # reshuffle the entries in one of them a bit
  # See [1], page 70 (section 2.3.3)
  it_list = np.copy(jer[k2, :])
  it_diff = np.zeros( (ne), np.int)
  ndiff = 0
  sign  = 1
  for k3 in range(0, ne):
    # Find index k4 in it_list with it_list[k4] == jer[k1,k3], and swap
    # it with it_list[k3]
    found = False
    k4 = 0
    while (k4 < ne and not found):
      if (jer[k1,k3] == it_list[k4]):
        found = True
        if (k4 != k3):
          sign = -sign
          # swap it_list[k4] and it_list[k3]
          it_list[k3], it_list[k4] = it_list[k4], it_list[k3]
      k4 += 1
    # If there was no such index (see above), save this in it_diff[]
    if (not found):
      it_diff[ndiff] = k3
      ndiff += 1
  return (ndiff, sign, it_list, it_diff)

# calculate one element of the Hamiltonian on the fly
def gen_simple(repulsion_energy, ne_up, ne_dn, jer, one_e_ham, two_e_ham, k1, k2):
  ne = ne_up + ne_dn
  ndiff, sign, it_list, it_diff = orb_difference(ne_up, ne_dn, jer, k1, k2)

  # sum up one- and two-electron contributions
  ret = 0
  if (ndiff == 0):
    ret += repulsion_energy
    for k3 in range(0, ne):
      ret += one_e_ham[jer[k1,k3], jer[k1,k3]]
      for k4 in range(0, ne):
        ret +=       - two_e_ham[jer[k1,k3],jer[ 0,k4],jer[k1,k3],jer[ 0,k4]] \
               + 0.5 * two_e_ham[jer[k1,k3],jer[k1,k4],jer[k1,k3],jer[k1,k4]]
  elif (ndiff == 1):
    i = it_diff[0]
    j = it_list[i]
    ret += one_e_ham[jer[k1,i], j]
    for k3 in range(0, ne):
      ret += - two_e_ham[jer[k1, i],jer[ 0,k3], j,jer[ 0,k3]] \
             + two_e_ham[jer[k1, i],jer[k1,k3], j,jer[k1,k3]]
  elif (ndiff == 2):
    ret += two_e_ham[jer[k1, it_diff[0]], jer[k1, it_diff[1]], \
                     it_list[it_diff[0]], it_list[it_diff[1]]]
  return ret * sign

# enable doctest
if __name__ == "__main__":
  import doctest
  doctest.testmod()

