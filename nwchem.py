import os, sys

# run nwchem
def run(parameters):
  # construct nwchem input file
  nwinputfile = 'nwchem.input'
  try:
    with open(nwinputfile, 'w') as f:
      f.write('''
start tce

geometry units angstroms
 symmetry %(symmetry)s
%(atoms)s
end

basis
%(basis)s
end

scf
 thresh 1.0e-10
 tol2e 1.0e-10
 singlet
 rhf
end

tce
 ccsd
 tilesize 5
 thresh 1.0d-6
end

task tce energy
''' % {'symmetry': parameters['symmetry'],
       'atoms'   : parameters['atoms'],
       'basis'   : parameters['basis']})
  except Exception as e:
    print("Could not open file '"+nwinputfile+"' for writing. Aborting.")
    print(e)
    sys.exit(1)

  # construct command line to run nwchem
  command = ('cd ' + parameters['outdir'] + ' && ' +
             parameters['nwchem'] + ' ' + nwinputfile + ' > nwchem.stdout')
  print(command)
  os.system(command)
