import sys, os.path
import numpy as np

if (sys.version_info.major > 2):
  import configparser as cp
else:
  import ConfigParser as cp

### Parameters

known_parameters = {
  'version'     : {'type': 'int'},

  'indir'       : {'type': 'string'},
  'nwchem'      : {'type': 'string', 'default': None},
  'symmetry'    : {'type': 'string'},
  'atoms'       : {'type': 'string'},
  'basis'       : {'type': 'string'},
  'calc_rho'    : {'type': 'boolean', 'default': True},
  'debug_rho'   : {'type': 'boolean', 'default': False},
  'ne_up'       : {'type': 'int'},
  'ne_down'     : {'type': 'int'},
  'E_frozen'    : {'type': 'float', 'default': -999999.},

  'diagonalize' : {'type': 'boolean', 'default': True},

  'outdir'      : {'type': 'string', 'default' : './'},
  }

def read_parameters(filename):
  config = cp.ConfigParser()
  parameters = {}
  if len(config.read(filename)) == 0:
    print("Could not read file '"+filename+"'.")
    sys.exit(1)
  for section in config.sections():
    for (key, value) in config.items(section):
      if key in known_parameters:
        if known_parameters[key]['type'] == 'int':
          parameters[key] = config.getint(section, key)
        elif known_parameters[key]['type'] == 'float':
          parameters[key] = config.getfloat(section, key)
        elif known_parameters[key]['type'] == 'string':
          parameters[key] = config.get(section, key)
        elif known_parameters[key]['type'] == 'boolean':
          string = config.get(section, key)
          if string == 'True' or string == 'true' or string == '1':
            parameters[key] = True
          else:
            parameters[key] = False
      else:
        print("Unknown parameter '"+key+"' in '"+filename+"'.")
        sys.exit(1)
  for parameter in known_parameters:
    if not parameter in parameters:
      if 'default' in known_parameters[parameter]:
        parameters[parameter] = known_parameters[parameter]['default']
      else:
        print("Parameter '"+parameter+"' not found in '"+filename+"', and "
              "no default set.")
        sys.exit(1)
    # Here we could check some of the parameters a little more
  return parameters

### Other input files

def convDtoF(s):
  return float(s.decode('utf-8').replace('D','E'))

def read_repulsion_energy(parameters):
  return np.loadtxt(parameters["indir"]+"repulsion_energy.dat", usecols=(0,))

def read_symmetry_orbitals(parameters):
  return np.loadtxt(parameters["indir"]+"symmetry.dat", usecols=(1,), dtype=(int))

# inefficient, but works for now
def read_one_electron_ham(parameters, E_orbital_n_both):
  one_e_ham = np.zeros((E_orbital_n_both, E_orbital_n_both))
  tmp = np.genfromtxt(parameters["indir"]+"one_electron_integral.dat", 
                      usecols=(0,1,2), converters={0:convDtoF},
                      dtype=(float, int, int))
  for element in tmp:
    one_e_ham[element[1]-1, element[2]-1] = element[0]
  return one_e_ham


# inefficient, but works for now
def read_two_election_ham(parameters, E_orbital_n_both):
  two_e_ham = np.zeros((E_orbital_n_both, E_orbital_n_both,\
                        E_orbital_n_both, E_orbital_n_both,))
  tmp = np.genfromtxt(parameters["indir"]+"two_electron_integral.dat", 
                      usecols=(0,1,2,3,4), converters={0:convDtoF},
                      dtype=(float, int, int, int, int))
  for element in tmp:
    two_e_ham[element[1]-1, element[2]-1, element[3]-1, element[4]-1] = element[0]
  return two_e_ham

