import numpy as np

symmetry_reduction = np.array([1], dtype=np.int)

# symmetry_reduction[a,b] is a table of the symmetry of the product of
# a and b, according to the C_{2v} group
# (see, e.g., http://www.webqc.org/symmetrypointgroup-c2v.html)
# a, b and the returned value are integers, denoting a symmetry.
# The symmetry integers come from definitions in nwchem (hardcoded),
#  see (src/symmetry/sym_char_tab.F).
#  These are:   symmetry  int-code
#                  A1        0
#                  A2        1
#                  B1        2
#                  B2        3
# The product table this function implements is:
#       | A1  A2  B1  B2
#    ---+---------------
#    A1 | A1  A2  B1  B2
#    A2 | A2  A1  B2  B1
#    B1 | B1  B2  A1  A2
#    B2 | B2  B1  A2  A1
symmetry_reduction_c2v = np.array([[0,1,2,3],
                                   [1,0,3,2],
                                   [2,3,0,1],
                                   [3,2,1,0]], dtype=np.int)

# symmetry_reduction_d2h[a,b] is a table of the symmetry of the product of
# a and b, according to the D_{2h} group
# (see, e.g., http://www.webqc.org/symmetrypointgroup-d2h.html)
# a, b and the returned value are integers, denoting a symmetry.
# The symmetry integers come from definitions in nwchem (hardcoded),
#  see (src/symmetry/sym_char_tab.F).
#  These are:   symmetry  int-code
#                  Ag        0
#                  Au        1
#                  B1g       2
#                  B1u       3
#                  B2g       4
#                  B2u       5
#                  B3g       6
#                  B3u       7
# The product table this function implements is:
#      | Ag  Au  B1g B1u B2g B2u B3g B3u
# -----+-----------------------------------
#  Ag  | Ag  Au  B1g B1u B2g B2u B3g B3u
#  Au  | Au  Ag  B1u B1g B2u B2g B3u B3g
#  B1g | B1g B1u Ag  Au  B3g B3u B2g B2u
#  B1u | B1u B1g Au  Ag  B3u B3g B2u B2g
#  B2g | B2g B2u B3g B3u Ag  Au  B1g B1u
#  B2u | B2u B2g B3u B3g Au  Ag  B1u B1g
#  B3g | B3g B3u B2g B2u B1g B1u Ag  Au
#  B3u | B3u B3g B2u B2g B1u B1g Au  Ag
symmetry_reduction_d2h = np.array([[0,1,2,3,4,5,6,7],
                                   [1,0,3,2,5,4,7,6],
                                   [2,3,0,1,6,7,4,5],
                                   [3,2,1,0,7,6,5,4],
                                   [4,5,6,7,0,1,2,3],
                                   [5,4,7,6,1,0,3,2],
                                   [6,7,4,5,2,3,0,1],
                                   [7,6,5,4,3,2,1,0]], dtype=np.int)


# return symmetry reduction table
def get_symmetry_reduction(sym):
  if sym == 'c2v':
    print("Using c2v symmetry")
    return symmetry_reduction_c2v
  elif sym == 'd2h':
    print("Using d2h symmetry")
    return symmetry_reduction_d2h
  elif sym == 'none':
    print("Not using symmetry")
    return symmetry_reduction
  print("Unknown symmetry '"+sym+"'")
  sys.exit(1)

