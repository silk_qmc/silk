License
-------

The SiLK QMC code is an distributed under the terms of the Educational Community License (ECL) 2.0

Copyright (c) 2012-2015: Louisiana State University
  Frank Löffler <knarf@cct.lsu.edu>
  Randall Hall <randall.hall@dominican.edu>
  Karol Kowalski <karol.kowalski@pnnl.gov>
  Mark Jarrell <jarrellphysics@gmail.com>,
  Juana Moreno <moreno@phys.lsu.edu>
  Xiaoyao Ma <maxiaoyao@gmail.com>

This software and its documentation were developed at Louiana State University.

Licensed under the Educational Community License, Version 2.0 (the "License"); you may
not use this file except in compliance with the License. You may obtain a copy of the
License at http://www.osedu.org/licenses/ECL-2.0

Unless required by applicable law or agreed to in writing, software distributed under the
License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
either express or implied. See the License for the specific language governing
permissions and limitations under the License.


Instructions for usage on supermike:
------------------------------------

* login to supermike
* git clone is available at `/project/knarf/silk/silk_git`. If you can't read that, contact knarf@cct.lsu.edu. If you need to write, make a copy (for now).
* nwchem not setup yet (make sure you got nwchem output files)

General instructions:

* main executable: [silk.py](silk.py)
    * expects one argument: an input file containing parameters. Example: [test_H2O_STO3G_c2v.par](test_H2O_STO3G_c2v.par)
    * input file specifies problem to work on, most of all, specifies input directory with lots of more files it needs to read, including the related nwchem input file, e.g., [input/H2O_STO3G_c2v/H2O_STO3G_c2v.nw](input/H2O_STO3G_c2v/H2O_STO3G_c2v.nw)
    * [silk.py](silk.py) sets up the main computational things, and at least as long as we develop, also contains what is actually done (evolution)
* [fact.py](fact.py): only a small helper for a fast (cached) factorial function
* [nwchem.py](nwchem.py): Given paramters from input file, runs nwchem. If you got the output file from nwchem already, you don't need this.
* [sinput.py](sinput.py): parse the input file and other files to read (nwchem output). There is nothing really interesting to see here. It should do what you would expect, but feel free...
* [symmetry.py](symmetry.py): contains matrices for common molecular symmetries, and actually much more documentation than code.
* [calc_rho.py](calc_rho.py): The **meat**. Calculates all internal data structures, like obtaining a list of spin_orbitals given an initial list and a wanted number of excitations, the "difference" between orbitals, or single elements of an Hamiltonian. Used by [silk.py](silk.py) as needed.

Example output:

    $ ./silk.py test_H2O_STO3G_c2v.par 
    Using c2v symmetry
    Number of orbitals: 7
    Silk: setup spin-orbitals done
    Number of states: 64
    Silk: calculating rho reduced
    Silk: Ham: 64x64 (4096) 
    Silk: 1968/4096 Hamiltonian elements non-zero (48.05%)
    Lowest eigenvalue: -74.9987058854
