#!/usr/bin/env python
from __future__ import print_function
import sys
import numpy as np

import sinput
import nwchem
from symmetry import get_symmetry_reduction
from fact import fact
from calc_rho import iterate_spin_orbitals, gen_simple, orb_difference, symmetry_product

# Read parameters
parfile = 'test_H2O_STO3G_c2v.par'
if len(sys.argv) > 1:
  parfile = sys.argv[1]
else:
  print("Need a parameter file name as (only) argument.")
  sys.exit(1)
parameters = sinput.read_parameters(parfile)

# Set symmetry
symmetry_reduction = get_symmetry_reduction(parameters['symmetry'])

# Read energies
E_orbital        = np.loadtxt(parameters['indir']+"orbital_energy.dat", usecols=(1,))
E_orbital_n_both = len(E_orbital) # 2*nrows*ncols
ne_up = parameters['ne_up']
ne_dn = parameters['ne_down']

# concatenate occupied and unoccupied orbitals for spin-up
E_orbital = np.concatenate([E_orbital[0:ne_up],
                            E_orbital[ne_up+ne_dn:ne_dn+E_orbital_n_both//2]])
n_orbitals = len(E_orbital)
print("Number of orbitals: "+str(n_orbitals));

# remove frozen states
E_orbital          = E_orbital[E_orbital >= parameters['E_frozen']]
E_orbital_n_frozen = n_orbitals - len(E_orbital)

# run nwchem if requested
if parameters['nwchem']:
  parameters['indir'] = parameters['outdir']
  # TODO: check if required files are already there, and skip calculation if so
  nwchem.run(parameters)
# read various input files
repulsion_energy = sinput.read_repulsion_energy(parameters)
sym_orb          = sinput.read_symmetry_orbitals(parameters)
one_e_ham        = sinput.read_one_electron_ham(parameters, E_orbital_n_both)
two_e_ham        = sinput.read_two_election_ham(parameters, E_orbital_n_both)

# calculate some more commonly used variables
num_total = fact(len(E_orbital)) / fact(len(E_orbital)-2)/2

jer = None

# output occupied orbitals in some meaningful way
def print_states(jer, n_orbitals):
  for state in jer:
    for i in range(2*n_orbitals):
      if i in state:
        print("X ", end="")
      else:
        print("  ", end="")
    print("")
  print(jer)

for tmpd in range(0, 1):
  jer = iterate_spin_orbitals(jer, n_orbitals, ne_up, ne_dn, sym_orb, symmetry_reduction, limit_excitations=2)
  nstate = len(jer)
  print("Number of states: "+str(nstate))
  print("Silk: calculating rho reduced")

  #print_states(jer, n_orbitals)

  # Calculate Hamiltonian

  # Initialization to 0
  ham = np.zeros( (nstate, nstate), np.float)
  ham_nonzero = 0
  # Loop over all elements
  for i in range(0, len(jer[:,0])):
    for j in range(0, len(jer[:,0])):
      # Calculate each element one by one
      ham[i,j] = gen_simple(repulsion_energy, ne_up, ne_dn, jer, one_e_ham, two_e_ham, i, j)
      # Count how many are not close to zero (for debugging)
      if (abs(ham[i,j]) > 1.e-15):
        ham_nonzero += 1
  
  
  #for i in range(0,len(jer[:,0])):
  #  for j in range(0,len(jer[:,0])):
  #    print("ham ",i,j,ham[i,j],ham[j,i])

  # Some debug output
  print("Silk: Ham: %dx%d (%d) " % (len(jer[:,0]), len(jer[:,0]), len(jer[:,0])*len(jer[:,0])))
  print("Silk: %d/%d Hamiltonian elements non-zero (%.2f%%)" %
        (ham_nonzero, len(jer[:,0])**2, 100*float(ham_nonzero)/len(jer[:,0])**2))
  
  eigenvals, eigenvecs = np.linalg.eigh(ham)
  ham_diagonalized = np.diag(eigenvals)
  #print("Hamd: "+str(ham_diagonalized))
  print("Eigenvalues (unsorted): "+str(eigenvals))
  print("Eigenvalues (sorted)  : "+str(np.sort(eigenvals)))
  #print("Ham: "+str(ham))
  print("Lowest eigenvalue: "+str(np.sort(eigenvals)[0]))
  #print("Eigenvects (unsorted):\n"+str(eigenvecs))
  #print("Ground state eigvect ",eigenvecs[0,:])

  sum = 0.0
  for i in range(0, len(jer[:, 0])):
    sum = sum + eigenvecs[i, 0] * eigenvecs[i, 0]
    if abs(eigenvecs[i, 0]) > 0.01:
      print(eigenvecs[i, 0], jer[i, :])
  #print("Alternate ground state eigvect ",eigenvecs[:,0])
  print(sum)

  for i in range(0,len(jer[:,0])-1):
    for j in range(i+1,len(jer[:,0])):
      if(eigenvals[j] < eigenvals[i]):
        temp=eigenvals[j]
        eigenvals[j]=eigenvals[i]
        eigenvals[i]=temp
        for k in range(0,len(jer[:,0])):
          temp=eigenvecs[k,i]
          eigenvecs[k,i]=eigenvecs[k,j]
          eigenvecs[k,j]=temp

  nbig = 0

  for i in range(0, len(jer[:, 0]) - 1):
    if (abs(eigenvecs[i, 0]) > 1.e-6):
      nbig = nbig + 1

  print("Number of big coefficients = ", nbig)


  onee = np.zeros( (n_orbitals*2, n_orbitals*2), np.float)
  # loop over elements, creating 1-electron-matrix
  #print("len(jer:0) :", len(jer[:,0]))
  #print("sym_orb:", dict(enumerate(sym_orb)))
  print("symmetry_reduction\n", symmetry_reduction)
  print("Symmetries:", map(lambda x: symmetry_product(x, sym_orb, symmetry_reduction), jer))
  for i in range(0, len(jer[:,0])):
    for j in range(0, len(jer[:,0])):
      # get 'difference' between orbitals i and j
      ndiff, sign, it_list, it_diff = orb_difference(ne_up, ne_dn, jer, i, j)
      # some debug output
      #if ndiff == 0:
      #  print("%d <-> %d :  ndiff=%d  sym: %d" % (
      #        i, j, ndiff, symmetry_reduction[0, sym_orb[jer[i, it_diff[0]]]]))
      #else:
      #  print("%d <-> %d :  ndiff=%d, sym: %d <-> %d (%d <-> %d)" % (
      #        i, j, ndiff, symmetry_reduction[0, sym_orb[jer[i, it_diff[0]]]],
      #                     symmetry_reduction[0, sym_orb[it_list[it_diff[0]]]],
      #                     jer[i, it_diff[0]], it_list[it_diff[0]]))
      #  print(symmetry_product(jer[i, :], sym_orb, symmetry_reduction),
      #        symmetry_product(jer[0, :], sym_orb, symmetry_reduction))

      # i and j are actually the same
      if i == j:
        for k in range(0,len(jer[i,:])):
          m = jer[i,k]
          onee[m,m] += eigenvecs[i, 0] ** 2
      #    print("adding to matrix element ",i,i,m,m,onee[m,m],eigenvecs[i,0]**2)

      # i and j only differ by one (in terms of orbitals, not actual numbers)
      elif ndiff == 1:
        different_orbital1 = jer[i, it_diff[0]]
        different_orbital2 = it_list[it_diff[0]]
#        print("diff 1")
        # check that the symmetry is the same, and only include contributions in that case
        if (symmetry_reduction[0, sym_orb[different_orbital1]] ==
            symmetry_reduction[0, sym_orb[different_orbital2]]):
          onee[different_orbital1, different_orbital2] += eigenvecs[i,0] * eigenvecs[j,0] * sign
  #print("raw one e-")
  #print(str(onee))
  #onee_diagonalized = np.diag(onee)
  #onee_eigenvals, onee_eigenvecs = np.linalg.eigh(onee)
  #print("one e- diagonalized, plus eigenvals")
  #print(str(onee_diagonalized))
  #print(str(np.sort(onee_eigenvals)))


  n_virtual_up = E_orbital_n_both // 2 - ne_up
  # n_virtual_up=(n_virtual_up).astype(int)
  n_virtual_dn = E_orbital_n_both // 2 - ne_dn
  # n_virtual_dn=(n_virtual_dn).astype(int)

  for myspin in range(0, 1):
    print(myspin, 'top')
    spin_list = []
    if myspin == 0:
      for i in range(0, ne_up):
        spin_list = np.append(spin_list, i)
        spin_list = spin_list.astype(int)
      for i in range(0, n_virtual_up):
        j = ne_up + ne_dn + i
        spin_list = np.append(spin_list, j)
        spin_list = spin_list.astype(int)
    else:
      for i in range(0, ne_dn):
        spin_list = np.append(spin_list, i + ne_up)
        spin_list = spin_list.astype(int)
      for i in range(0, n_virtual_up):
        j = ne_up + ne_dn + i
        spin_list = np.append(spin_list, j)
        spin_list = spin_list.astype(int)
  print("spin_list", spin_list, spin_list.shape)

  onee_new = np.zeros((7, 7))
  for i in range(0, E_orbital_n_both // 2):
    k = spin_list[i]
    for j in range(0, E_orbital_n_both // 2):
      l = spin_list[j]
      onee_new[i, j] = onee[k, l]
  print('onee_new 7x7', onee_new.shape, onee_new)

  onee_diagonalized = np.diag(onee_new)
  # onee_diagonalized=np.concatenate((onee_diagonalized,onee_diagonalized))
  print('diag', onee_diagonalized)

  onee_eigenvals_new, onee_eigenvecs_new = np.linalg.eigh(onee_new)

  print('eigen new', onee_eigenvals_new)

  onee_eigenvecs = np.zeros((14, 14))
  onee_eigenvals = np.zeros((14))
  # onee_eigenvecs_new=np.concatenate((onee_eigenvecs_new,onee_eigenvecs_new))
  # onee_eigenvals_new=np.concatenate((onee_eigenvals_new,onee_eigenvals_new))

  for i in range(0, 7):
    k = spin_list[i]
    onee_eigenvals[k] = onee_eigenvals_new[i]
    for j in range(0, 7):
      l = spin_list[j]
      onee_eigenvecs[k, l] = onee_eigenvecs_new[i, j]

  print('one eigenvals up', onee_eigenvals)
  print('one eigenvecs up', onee_eigenvecs)

  for myspin in range(0, 1):
    print(myspin, 'top')
    spin_list = []
    if myspin == 0:
      for i in range(ne_up, ne_up + ne_dn):
        spin_list = np.append(spin_list, i)
        spin_list = spin_list.astype(int)
      for i in range(0, n_virtual_up):
        j = ne_up + ne_dn + n_virtual_up + i
        spin_list = np.append(spin_list, j)
        spin_list = spin_list.astype(int)
    else:
      for i in range(0, ne_dn):
        spin_list = np.append(spin_list, i + ne_up)
        spin_list = spin_list.astype(int)
      for i in range(0, n_virtual_up):
        j = ne_up + ne_dn + i
        spin_list = np.append(spin_list, j)
        spin_list = spin_list.astype(int)
  print("spin_list", spin_list, spin_list.shape)

  for i in range(0, 7):
    k = spin_list[i]
    onee_eigenvals[k] = onee_eigenvals_new[i]
    for j in range(0, 7):
      l = spin_list[j]
      onee_eigenvecs[k, l] = onee_eigenvecs_new[i, j]

      print('one eigenvals both', onee_eigenvals)
      print('one eigenvecs both', onee_eigenvecs)

      # print("raw one e-")
      # print(str(onee))
      # onee_diagonalized = np.diag(onee)
      # onee_eigenvals, onee_eigenvecs = np.linalg.eigh(onee)

      #  print("check orthonormality ")
      #  for i in range(0,E_orbital_n_both):
      #    for j in range(0,E_orbital_n_both):
      #      if(abs(onee_eigenvecs[j,i]) < 1.e-16):
      #        onee_eigenvecs[j,i]=0.0


      #  for i in range(0,E_orbital_n_both):
      #    print("evecs ",onee_eigenvecs[:,i])
      #    for j in range(0,E_orbital_n_both):
      #      sum=0.0
      #      for k in range(0,E_orbital_n_both):
      #        sum=sum+onee_eigenvecs[k,i]*onee_eigenvecs[k,j]
      #      print(i,j,sum)

  print("one e- diagonalized, plus eigenvals")
  print(str(onee_diagonalized))
  print(str(np.sort(onee_eigenvals_new)))
  # print(onee_eigenvecs)

  # for i in range(0,5):
  # print("Orbital ",i," occupancy = ",onee_diagonalized[i],onee_diagonalized[i+5],onee_diagonalized[i]+onee_diagonalized[i+5])

  # for i in range(10,12):
  # print("Orbital ",i," occupancy = ",onee_diagonalized[i],onee_diagonalized[i+2],onee_diagonalized[i]+onee_diagonalized[i+2])

  
# prepare on-the-fly calculation of rho (setup spin orbinal configurations)
if (parameters["calc_rho"]):
  print("Silk: preparing on-the-fly calculation of rho")
  jer = iterate_spin_orbitals(jer, n_orbitals, ne_up, ne_dn, sym_orb, symmetry_reduction, limit_excitations=0)
  nstate = len(jer)
  print("Silk: %d/%d up/down-electrons, each on %d orbitals" % (ne_up, ne_dn, E_orbital_n_both/2))
else:
  print("Silk: loading rho not implemented yet")
  sys.exit(1)




if parameters['debug_rho'] or parameters['diagonalize']:
  print("Silk: calculating rho")
  ham = np.zeros( (nstate, nstate), np.float)

  ham_nonzero = 0
  for i in range(0, len(jer[:,0])):
    for j in range(0, len(jer[:,0])):
      ham[i,j] = gen_simple(repulsion_energy, ne_up, ne_dn, jer, one_e_ham, two_e_ham, i, j)
      if (abs(ham[i,j]) > 1.e-15):
        ham_nonzero += 1
  print("Silk: %d/%d Hamiltonian elements non-zero (%.2f%%)" %
        (ham_nonzero, len(jer[:,0])**2, 100*float(ham_nonzero)/len(jer[:,0])**2))
  np.savetxt(parameters["outdir"]+"/ham.dat", ham, '%.16g', delimiter="\n")

# calculate diagonal Hamiltonian directly
if parameters['diagonalize']:
  print("Silk: diagonalizing Hamiltonian directly")
  ham_diagonalized = np.diag(ham)
  eigenvals, eigenvecs = np.linalg.eig(ham)
  print("Eigenvalues: "+str(np.sort(eigenvals)[:5]))


for k1 in range(0, E_orbital_n_both):
  orb_en=one_e_ham[k1,k1]
  #for k2 in range(0, E_orbital_n_both//2):
    #orb_en=orb_en+2.0*two_e_ham[k2,k1,k2,k1]-two_e_ham[k2,k1,k1,k2]
  print('Orbital Energy: ' + str(k1)+ '= ' + str(orb_en))

one_e_ham_temp=np.zeros((E_orbital_n_both, E_orbital_n_both))

for k1 in range(0,E_orbital_n_both):
  for k2 in range(0,E_orbital_n_both):
    sum = 0.0
    for k3 in range(0,E_orbital_n_both):
      for k4 in range(0,E_orbital_n_both):
        sum=sum+onee_eigenvecs[k3,k1]*one_e_ham[k3,k4]*onee_eigenvecs[k4,k2]
    one_e_ham_temp[k1,k2]=sum
for k1 in range(0,E_orbital_n_both):
  for k2 in range(0,E_orbital_n_both):
    one_e_ham[k1,k2]=one_e_ham_temp[k1,k2]


#print("new one electron hamiltonian: " + str(one_e_ham))


two_e_ham_temp = np.zeros((E_orbital_n_both, E_orbital_n_both,\
                        E_orbital_n_both, E_orbital_n_both,))

#Splitting up "for" loop

#First loop, replacing j4 --> j8
for j1 in range(0,E_orbital_n_both):
  for j2 in range(0, E_orbital_n_both):
    for j3 in range(0, E_orbital_n_both):
      for j4 in range(0, E_orbital_n_both):
        sum=0.0
        for j8 in range(0, E_orbital_n_both):
          sum=sum+onee_eigenvecs[j8,j4]*two_e_ham[j1,j2,j3,j8]
        two_e_ham_temp[j1,j2,j3,j4]=sum
for j1 in range(0,E_orbital_n_both):
  for j2 in range(0,E_orbital_n_both):
    for j3 in range(0,E_orbital_n_both):
      for j4 in range(0,E_orbital_n_both):
        two_e_ham[j1,j2,j3,j4]=two_e_ham_temp[j1,j2,j3,j4]


#Second loop, replacing j3-->j7
for j1 in range(0,E_orbital_n_both):
  for j2 in range(0, E_orbital_n_both):
    for j3 in range(0, E_orbital_n_both):
      for j4 in range(0, E_orbital_n_both):
        sum=0.0
        for j7 in range(0, E_orbital_n_both):
          sum=sum+onee_eigenvecs[j7,j3]*two_e_ham[j1,j2,j7,j4]
        two_e_ham_temp[j1,j2,j3,j4]=sum
for j1 in range(0,E_orbital_n_both):
  for j2 in range(0,E_orbital_n_both):
    for j3 in range(0,E_orbital_n_both):
      for j4 in range(0,E_orbital_n_both):
        two_e_ham[j1,j2,j3,j4]=two_e_ham_temp[j1,j2,j3,j4]



#Third loop, replacing j2-->j6
for j1 in range(0,E_orbital_n_both):
  for j2 in range(0, E_orbital_n_both):
    for j3 in range(0, E_orbital_n_both):
      for j4 in range(0, E_orbital_n_both):
        sum=0.0
        for j6 in range(0, E_orbital_n_both):
          sum=sum+onee_eigenvecs[j6,j2]*two_e_ham[j1,j6,j3,j4]
        two_e_ham_temp[j1,j2,j3,j4]=sum
for j1 in range(0,E_orbital_n_both):
  for j2 in range(0,E_orbital_n_both):
    for j3 in range(0,E_orbital_n_both):
      for j4 in range(0,E_orbital_n_both):
        two_e_ham[j1,j2,j3,j4]=two_e_ham_temp[j1,j2,j3,j4]



#Final loop, replacing k1-->j5
for j1 in range(0,E_orbital_n_both):
  for j2 in range(0, E_orbital_n_both):
    for j3 in range(0, E_orbital_n_both):
      for j4 in range(0, E_orbital_n_both):
        sum=0.0
        for j5 in range(0, E_orbital_n_both):
          sum=sum+onee_eigenvecs[j5,j1]*two_e_ham[j5,j2,j3,j4]
        two_e_ham_temp[j1,j2,j3,j4]=sum

for j1 in range(0,E_orbital_n_both):
  for j2 in range(0,E_orbital_n_both):
    for j3 in range(0,E_orbital_n_both):
      for j4 in range(0,E_orbital_n_both):
        two_e_ham[j1,j2,j3,j4]=two_e_ham_temp[j1,j2,j3,j4]

#print("new two electron ham: ", two_e_ham)


print("one electron eigenvecs", onee_eigenvecs)



#orb_en_temp=[]

#for k1 in range(0, E_orbital_n_both):
#  orb_en=one_e_ham[k1,k1]
#  orb_en_temp=np.append(orb_en_temp,orb_en)
#  #for k2 in range(0, E_orbital_n_both//2):
#    #orb_en=orb_en+2.0*two_e_ham[k2,k1,k2,k1]-two_e_ham[k2,k1,k1,k2]

#  print('Orbital Energy Unsorted: ' + str(k1)+ '= ' + str(orb_en))
#orb_en_temp=np.sort((-1)*orb_en_temp)
#orb_en_temp=orb_en_temp*(-1)
#print('unsorted: ',orb_en_temp)
#orb_en_temp=np.sort(orb_en_temp)
#orb_en_temp=orb_en_temp*(-1)
#print("sorted: ", orb_en_temp)





index_orb=[]
orb_en_temp=[]
for i in range(0,E_orbital_n_both):
  index_orb=np.append(index_orb,(i))
  index_orb=index_orb.astype(int)
  orb_en_temp=np.append(orb_en_temp,one_e_ham[i,i])


#index_orb_j=[]
#for j in orb_en_temp:
#  index_orb_j=np.append(index_orb_j,(j))

#index_orb_i=[]
#for i in range(0,len(orb_en_temp)):
#  index_orb_i=np.append(index_orb_i,(i))
#  index_orb_i=index_orb_i.astype(int)
#index_orb=list(zip(index_orb_i,index_orb_j))



#Trevor look here
#print('before any sort ',index_orb,orb_en_temp)


for i in range(0,E_orbital_n_both-1):

  for j in range(i+1,E_orbital_n_both):
    if orb_en_temp[j] < orb_en_temp[i]:
      temp=orb_en_temp[i]
      orb_en_temp[i]=orb_en_temp[j]
      orb_en_temp[j]=temp
      itemp=index_orb[i]
      index_orb[i]=index_orb[j]
      index_orb[j]=itemp

#    if orb_en_temp[k2]<orb_en_temp[k1]:
      #temp=orb_en_temp[k1]
      #orb_en_temp[k1]=orb_en_temp[k2]
      #orb_en_temp[k2]=temp

#print('after  sort ',index_orb,orb_en_temp)

index_orb_nwchem=[]

for i in range(0,ne_up):
  j=index_orb[2*i]
  index_orb_nwchem=np.append(index_orb_nwchem,(j))
#  print(" i j ",i,j,one_e_ham[j,j])


print(index_orb_nwchem)


for i in range(0,ne_dn):
  j=index_orb[2*i+1]
  index_orb_nwchem=np.append(index_orb_nwchem,(j))

#  print(" i j ",i,j,one_e_ham[j,j])


#print(index_orb_nwchem)


n_virtual_up=E_orbital_n_both//2-ne_up
#n_virtual_up=(n_virtual_up).astype(int)
n_virtual_dn=E_orbital_n_both//2-ne_dn
#n_virtual_dn=(n_virtual_dn).astype(int)

for i in range(0,n_virtual_up):
  j=index_orb[ne_up+ne_dn+2*i]
  index_orb_nwchem=np.append(index_orb_nwchem,(j))
#  print(" i j ",i,j,one_e_ham[j,j])
print(index_orb_nwchem)



for i in range(0,n_virtual_dn):
  j=index_orb[ne_up+ne_dn+2*i+1]
  index_orb_nwchem=np.append(index_orb_nwchem,(j))
#  print(" i j ",i,j,one_e_ham[j,j])
#print(index_orb_nwchem)

for i in range(0,E_orbital_n_both):
  j=index_orb_nwchem[i].astype(int)
  print("i j ",i,j,one_e_ham[j,j])


for i1 in range(0,E_orbital_n_both):
  j1=index_orb_nwchem[i1].astype(int)
  for i2 in range(0,E_orbital_n_both):
    j2=index_orb_nwchem[i2].astype(int)
    one_e_ham_temp[i1,i2]=one_e_ham[j1,j2]

for i1 in range(0,E_orbital_n_both):
  for i2 in range(0,E_orbital_n_both):
    one_e_ham[i1,i2]=one_e_ham_temp[i1,i2]


#    print(" 2e ",two_e_ham[i1,i2,i1,i2],two_e_ham[i1,i2,i2,i1])



for i1 in range(0,E_orbital_n_both):
  j1=index_orb_nwchem[i1].astype(int)
  for i2 in range(0,E_orbital_n_both):
    j2=index_orb_nwchem[i2].astype(int)
    for i3 in range(0,E_orbital_n_both):
      j3=index_orb_nwchem[i3].astype(int)
      for i4 in range(0,E_orbital_n_both):
        j4=index_orb_nwchem[i4].astype(int)
        two_e_ham_temp[i1,i2,i3,i4]=two_e_ham[j1,j2,j3,j4]


for i1 in range(0,E_orbital_n_both):
  for i2 in range(0,E_orbital_n_both):
    for i3 in range(0,E_orbital_n_both):
      for i4 in range(0,E_orbital_n_both):
        two_e_ham[i1,i2,i3,i4]=two_e_ham_temp[i1,i2,i3,i4]

#  print('Orbital Energy Unsorted: ' + str(k1)+ '= ' + str(orb_en))



#final_index=[]
#j=0
#for i in index_orb_new:
##  index_orb_i=np.append(index_orb_i,(i))
#  final_index=np.append(final_index,i[0])
#  j=j+1
#  print(' i ',i[0],final_index[j-1])



# Initialization to 0
for i in range(0,len(jer[:,0])):
  print(i,jer[i,:])





ham = np.zeros( (nstate, nstate), np.float)
ham_nonzero = 0
# Loop over all elements
for i in range(0, len(jer[:,0])):
  print(gen_simple(repulsion_energy, ne_up, ne_dn, jer, one_e_ham, two_e_ham, i, i),jer[i,:])
  for j in range(0, len(jer[:,0])):
    # Calculate each element one by one
    ham[i,j] = gen_simple(repulsion_energy, ne_up, ne_dn, jer, one_e_ham, two_e_ham, i, j)

    # Count how many are not close to zero (for debugging)
    if (abs(ham[i,j]) > 1.e-15):
      ham_nonzero += 1



#for i in range(0,len(jer[:,0])):
#  for j in range(0,len(jer[:,0])):
#    print("ham ",i,j,ham[i,j],ham[j,i])


# Some debug output
print("Silk: Ham: %dx%d (%d) " % (len(jer[:,0]), len(jer[:,0]), len(jer[:,0])*len(jer[:,0])))
print("Silk: %d/%d Hamiltonian elements non-zero (%.2f%%)" %
      (ham_nonzero, len(jer[:,0])**2, 100*float(ham_nonzero)/len(jer[:,0])**2))


eigenvals, eigenvecs = np.linalg.eigh(ham)
ham_diagonalized = np.diag(eigenvals)
#print("Hamd: "+str(ham_diagonalized))
print("Eigenvalues (unsorted): "+str(eigenvals))
print("Eigenvalues (sorted)  : "+str(np.sort(eigenvals)))
#print("Ham: "+str(ham))
print("Lowest eigenvalue: "+str(np.sort(eigenvals)[0]))
# calculate diagonal Hamiltonian directly
if parameters['diagonalize']:
  print("Silk: diagonalizing Hamiltonian directly")
  ham_diagonalized = np.diag(ham)
  eigenvals, eigenvecs = np.linalg.eig(ham)
  print("Eigenvalues: "+str(np.sort(eigenvals)[:5]))


for i in range(0,len(jer[:,0])-1):
  for j in range(i+1,len(jer[:,0])):
    if(eigenvals[j] < eigenvals[i]):

      temp=eigenvals[j]
      eigenvals[j]=eigenvals[i]
      eigenvals[i]=temp
      for k in range(0,len(jer[:,0])):
        temp=eigenvecs[k,i]
        eigenvecs[k,i]=eigenvecs[k,j]
        eigenvecs[k,j]=temp



#print(eigenvals[0])
#for i in range(0,len(jer[:,0])):
#  if abs(eigenvecs[i,0]) > 0.01:
#    print(eigenvecs[i,0],jer[i,:])

#nbig1=0

#for i in range(0,len(jer[:,0])-1):
#  if(abs(eigenvecs[i,0]) > 1.e-6):
#    nbig1=nbig1+1
#print("Number of big coefficients = ",nbig,nbig1)

#for k in range(0,len(jer[:,0])):
#  sum=0.0
#  for i in range(0,len(jer[:,0])):
#    for j in range(0,len(jer[:,0])):
#      sum=sum+ gen_simple(repulsion_energy, ne_up, ne_dn, jer, one_e_ham, two_e_ham, i, j)*eigenvecs[i,k]*eigenvecs[j,k]

#  print(k,sum)
