import math

# cache for calculating factorials
fact_cache = {}
def fact(x):
  return fact_cache.setdefault(x, math.factorial(x))

